﻿using UnityEngine;
using System.Collections;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody2D rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    private float direction;
    private float speed = 50;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
        OutOfBounds();
    }
    private void FixedUpdate()
    {
        Time.fixedDeltaTime = 0.01f;
    }
    private void GetInput()
    {
        //Moove();
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            gameObject.SetActive(false);
            Controller_Enemy.enemyVelocity = 0;
            collision.gameObject.SetActive(false);
            Controller_Hud.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }
    //private void Moove()
    //{
    //    direction = Input.GetAxisRaw("Horizontal");
    //    rb.velocity = new Vector3(direction * speed, rb.velocity.y, 0);
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("nafta"))
        {           
            LifeScript.Curar(15);
            StartCoroutine(DesactivarObjeto(collision.gameObject));
        }
        if (collision.gameObject.CompareTag("antiNafta"))
        {
            LifeScript.Damage(15);
            StartCoroutine(DesactivarObjeto(collision.gameObject));
        }
    }
    IEnumerator DesactivarObjeto(GameObject g)
    {
        g.SetActive(false);

        float tiempoActual = Time.time;
        while (Time.time < tiempoActual + 6)
        {
            yield return null;
        }
        g.SetActive(true);
    }
    public void OutOfBounds()
    {
        if (this.transform.position.x <= -48.2)
        {
           Controller_Hud.gameOver=true;
           Controller_Instantiator.CambiarVelocidad = false;
        }
    }

}

