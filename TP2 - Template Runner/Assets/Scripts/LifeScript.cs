using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeScript : MonoBehaviour
{
    [SerializeField] public static float life;
    [SerializeField] private static float maxlife;
    [SerializeField] private Slider slider;
    private float StartLife;
    void Start()
    {
        maxlife = 15;
        life = maxlife;
        StartLife = life;
        slider = FindObjectOfType<Slider>();
        CambiarvidaMaxima(maxlife);
    }

    public void CambiarvidaMaxima(float vidaMaxima)
    {
        slider.maxValue = vidaMaxima;
    }

    public void CambiarVidaActual(float vidaActual)
    {
        slider.value = vidaActual;
    }

    private void Update()
    {
        life = life - Time.deltaTime;
        CambiarVidaActual(life);
        if (life <= 0)
        {
            Controller_Hud.gameOver = true;
            Controller_Instantiator.CambiarVelocidad = false;
        }
    }

    public void ReiniciarVida()
    {
        life = StartLife;
    }

    public static void Curar(int vidaAgregada)
    {
        if (life + vidaAgregada >= maxlife)
        {
            life = maxlife;
        }
        else life += vidaAgregada;
    }
    public static void Damage(int vidaRestada)
    {
        life -= vidaRestada;
    }
}
