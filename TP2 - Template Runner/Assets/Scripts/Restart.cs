﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class Restart : MonoBehaviour
{
    private Vector3 posInicial;
    GameObject jugador;
    GameObject Instantiator;
    GameObject[] arrayEnemigos;
    Controller_Hud hud;
    private void Start()
    {
        jugador = GameObject.Find("Player");
        Instantiator = GameObject.Find("Instantiator");
        posInicial = jugador.GetComponent<Transform>().position;
        hud = FindObjectOfType<Controller_Hud>();
    }
    void Update()
    {
        GetInput();
    }
    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            ReinicioPlayer();
            ReiniciarBarraDeVida();
            DesactivarEnemigos();
            ReinicioHUD();
            Controller_Instantiator.CambiarVelocidad = true;
        }
    }

    private void ReiniciarBarraDeVida()
    {
        jugador.GetComponent<LifeScript>().ReiniciarVida();
    }

    private void DesactivarEnemigos()
    {
        arrayEnemigos = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemies in arrayEnemigos)
        {
            enemies.SetActive(false);
        }
    }

    private void ReinicioPlayer()
    {
        jugador.SetActive(true);
        jugador.transform.position = posInicial;
        jugador.GetComponent<Gravedad>().gravityScale = -20;
        jugador.GetComponent<Gravedad>().targetGravityScale = -20;
    }

    private void ReinicioHUD()
    {
        Controller_Hud.gameOver = false;
        hud.Distance = 0f;
        hud.gameOverText.gameObject.SetActive(false);
        Controller_Hud.MaxScoreText.gameObject.SetActive(false);
        hud.panel.SetActive(false);
    }

}
