﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    [SerializeField] GameObject texto;
    public Text gameOverText;
    public static Text MaxScoreText;
    public GameObject panel;
    public static float MaxScore = 0;
    private float distance = 0;
    public float distanceRounded = 0;
    public float MaxdistanceRounded = 0;
    private ScoreManager scoreManager;

    public float Distance { get => distance; set => distance = value; }

    void Start()
    {
        MaxScoreText = texto.GetComponent<Text>();
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
        scoreManager = new ScoreManager();
    }

    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            distanceRounded = (float)System.Math.Round(distance, 2, System.MidpointRounding.AwayFromZero);
            MaxdistanceRounded = (float)System.Math.Round(MaxScore, 2, System.MidpointRounding.AwayFromZero);
            gameOverText.text = "Game Over \n Total Distance: " + distanceRounded.ToString();
            gameOverText.gameObject.SetActive(true);
            MaxScoreText.text = "Record: " + MaxdistanceRounded.ToString();
            panel.SetActive(true);
            MaxScoreText.gameObject.SetActive(true);
            scoreManager.CompararDistancias(distance);
            scoreManager.CargarDatos();
        }
        else
        {
            distance += Time.deltaTime;
            distanceRounded = (float)System.Math.Round(distance, 2);
            distanceText.text = distanceRounded.ToString();
        }
    }
    public static void Actualizar(float nuevoScore)
    {
        if (MaxScoreText != null)
        {
            MaxScore = (float)System.Math.Round(nuevoScore, 2, System.MidpointRounding.AwayFromZero); ;
            MaxScoreText.text = "MAX SCORE: " + MaxScore.ToString();
        }
    }
}
