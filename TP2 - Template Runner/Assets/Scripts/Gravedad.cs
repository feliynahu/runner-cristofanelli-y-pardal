using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravedad : MonoBehaviour
{
    public float gravityScale = 40f;
    public float targetGravityScale = -40f;
    public float gravityChangeSpeed = 2f;
    private Rigidbody2D rb;
    private bool isGravityInverted = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = Vector2.zero;
            isGravityInverted = !isGravityInverted; 

            if (isGravityInverted)
            {
                targetGravityScale = -40f; 
            }
            else
            {
                targetGravityScale = 40f;
            }
        }
        
     
        gravityScale = Mathf.Lerp(gravityScale, targetGravityScale, Time.deltaTime * gravityChangeSpeed);
        Physics2D.gravity = new Vector2(0f, gravityScale * 9.81f); 
    }

}
