﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    [SerializeField]public static float enemyVelocity;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0), ForceMode2D.Force);
        OutOfBounds();
    }
    private void OnDisable()
    {
        enemyVelocity = 0;
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -100 && this.gameObject.CompareTag("Enemy"))
        {
            this.gameObject.SetActive(false);
        }
    }
}
