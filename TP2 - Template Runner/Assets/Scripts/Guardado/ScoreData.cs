[System.Serializable]
public class ScoreData 
{
    public float score;

    public ScoreData(float score)
    {
        this.score = score;
    }
}
