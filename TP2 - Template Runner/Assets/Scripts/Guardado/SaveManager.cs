using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager 
{
    public static void SaveData(float score)
    {
        ScoreData scoreData = new ScoreData(score);
        string dataPath = Application.persistentDataPath + "/score.save";
        FileStream fileStream = new FileStream(dataPath, FileMode.Create);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, scoreData);
        fileStream.Close();
    }

    public static float LoadData()
    {
        string dataPath = Application.persistentDataPath + "/score.save";
        if (File.Exists(dataPath))
        {
            FileStream fileStream = new FileStream(dataPath, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            ScoreData scoreData = (ScoreData)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            return scoreData.score;
        }
        else
        {
            return 0f;
        }
    }
}
