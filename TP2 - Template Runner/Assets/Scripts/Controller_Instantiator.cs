﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> TypeOfEnemies;
    public List<GameObject> ObstaclesLists;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;
    [SerializeField] private float yMax = 0;
    [SerializeField] private float yMin = 0;
    public static bool CambiarVelocidad;
    private float VelocidadAumento = 10f;
    void Start()
    {
        AddPrefabs();
        CambiarVelocidad = true;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }
    //private void IniatiliazeVelocity()
    //{
    //    time += Time.deltaTime;
    //    Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 60f);
    //}
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        if (CambiarVelocidad)
        {
            VelocidadAumento = Mathf.SmoothStep(10f, 30f, time / 60f);
            Controller_Enemy.enemyVelocity = VelocidadAumento;
        }
        else
        {
            VelocidadAumento = 10f;
            Controller_Enemy.enemyVelocity = VelocidadAumento;
        }
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            GameObject obstaculo = GiveObject();
            if (obstaculo != null)
            {
                obstaculo.transform.position = new Vector3(instantiatePos.transform.position.x, UnityEngine.Random.Range(yMin, yMax), instantiatePos.transform.position.z);
                respawningTimer = UnityEngine.Random.Range(5, 6);
            }
            else { respawningTimer = 0; }
        }
    }

    private void AddPrefabs()
    {
        for (int i = 0; i < TypeOfEnemies.Count; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                GameObject g = Instantiate(TypeOfEnemies[i]);
                g.SetActive(false);
                ObstaclesLists.Add(g);
                g.transform.parent = transform;
            }
        }

    }
    public GameObject GiveObject()
    {
        int rnd = Random.Range(0, ObstaclesLists.Count);
        for (int i = 0; i < ObstaclesLists.Count; i++)
        {
            if (!ObstaclesLists[rnd].activeSelf)
            {
                print(rnd);
                ObstaclesLists[rnd].SetActive(true);
                return ObstaclesLists[rnd];
            }
        }
        return null;
    }
}
