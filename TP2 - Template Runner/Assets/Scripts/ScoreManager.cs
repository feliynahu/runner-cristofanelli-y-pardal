using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    ScoreData scoreData;
    [SerializeField] GameObject obj;
    //[SerializeField] Controller_Hud hud;
    private void Start()
    {
        /*  else {*/
        //hud = obj.GetComponent<Controller_Hud>();
        CargarDatos(); /*}*/



    }
    private void Update()
    {
        //InputCharge();
    }
    public void CompararDistancias(float score)
    {
        float scoreData = SaveManager.LoadData();
        if (score > SaveManager.LoadData())
        {
            guardarScore(score);
            //CargarDatos();
        }
    }
    private void guardarScore(float score)
    {
        SaveManager.SaveData(score);
    }
    //private void InputCharge()
    //{
    //    if (Input.GetKeyDown(KeyCode.R)) CargarDatos();
    //}


    public void CargarDatos()
    {
        float scoreData = SaveManager.LoadData();
        if(scoreData != 0f){ Controller_Hud.Actualizar(scoreData); }
        else { Controller_Hud.Actualizar(0); }
    }
}
